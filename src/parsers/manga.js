import cheerio from "cheerio";

import { MANGA_ELEMENTS } from "../config/mangareader/source.constants.js";
import { parseImage, parseText } from "../utils/parser.js";
import { BASE_PATH } from "../config/router.js";

export function parseMangaView(data, mangaName) {
  const $ = cheerio.load(data);
  const el = $(MANGA_ELEMENTS.RESULTS);

  const title = parseText(el.find(MANGA_ELEMENTS.TITLE));
  const alternateTitle = parseText(el.find(MANGA_ELEMENTS.ALTERNATE_TITLE));
  const year = parseInt(parseText(el.find(MANGA_ELEMENTS.YEAR)));
  const status = parseText(el.find(MANGA_ELEMENTS.STATUS));
  const author = parseText(el.find(MANGA_ELEMENTS.AUTHOR));
  const artist = parseText(el.find(MANGA_ELEMENTS.ARTIST));
  const description = parseText(el.find(MANGA_ELEMENTS.DESCRIPTION));
  const image = parseImage(el.find(MANGA_ELEMENTS.IMAGE), "src");
  const genres = getGenres($, el.find(MANGA_ELEMENTS.GENRES));
  const chapters = parseChapters(
    $,
    el.find(MANGA_ELEMENTS.CHAPTERS),
    mangaName
  );

  return {
    title,
    alternateTitle,
    year,
    status,
    author,
    artist,
    genres,
    description,
    image,
    chapters,
  };
}

function getGenres(document, element) {
  let genres = [];
  element.each((index, genre) => {
    genres[index] = parseText(document(genre));
  });
  return genres;
}

function parseChapters(document, element, mangaName) {
  let chapters = [];
  element.each((index, chapter) => {
    const chapterElement = document(chapter);
    const number = parseInt(
      parseText(chapterElement.find(MANGA_ELEMENTS.CHAPTER_NUMBER))
        .split(" ")
        .pop()
    );

    chapters[index] = {
      number,
      name: chapterElement
        .find(MANGA_ELEMENTS.CHAPTER_NAME)
        .text()
        .split(":")[1]
        .trim(),
      date: parseText(chapterElement.find(MANGA_ELEMENTS.CHAPTER_DATE)),
      link: `${BASE_PATH}/${mangaName}/${number}/1`,
    };
  });

  return chapters;
}
