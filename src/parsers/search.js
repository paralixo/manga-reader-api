import cheerio from "cheerio";

import { SEARCH_ELEMENTS } from "../config/mangareader/source.constants.js";
import { parseAttribute, parseImage, parseText } from "../utils/parser.js";

export function parseSearchView(data) {
  const $ = cheerio.load(data);
  const $searchResults = $(SEARCH_ELEMENTS.RESULTS);

  let mangas = [];
  $searchResults.each((index, element) => {
    const el = $(element);
    const name = parseText(el.find(SEARCH_ELEMENTS.TITLE));
    const image = parseImage(el.find(SEARCH_ELEMENTS.IMAGE), "data-src");
    const url = parseAttribute(el.find(SEARCH_ELEMENTS.TITLE), "href");
    const chapters = parseInt(
      parseText(el.find(SEARCH_ELEMENTS.CHAPTERS)).split(" ")[0]
    );

    // TODO: pagination
    mangas.push({
      name,
      image,
      chapters,
      url,
    });
  });

  return mangas;
}
