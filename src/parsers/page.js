import cheerio from "cheerio";
import axios from "axios";

import { parseAttribute, parseImage } from "../utils/parser.js";
import {
  BASE_URL,
  PAGE_ELEMENT,
} from "../config/mangareader/source.constants.js";
import { BASE_PATH } from "../config/router.js";

export async function parsePageView(data, mangaName, chapter, page) {
  const $ = cheerio.load(data);
  const $page = $(PAGE_ELEMENT);

  const image = parseImage($page, "src");
  const width = parseInt(parseAttribute($page, "width"));
  const height = parseInt(parseAttribute($page, "height"));
  const previous = await getPreviousPage(mangaName, chapter, page);
  const next = await getNextPage(mangaName, chapter, page);

  return {
    image,
    width,
    height,
    next,
    previous,
  };
}

async function getNextPage(mangaName, chapter, page) {
  let next = `${BASE_PATH}/${mangaName}`;

  await axios
    .get(`${BASE_URL}/${mangaName}/${chapter}/${page + 1}`)
    .then(async (response) => {
      const pageAskedBySource = parseInt(
        response.request.path.split("/").pop()
      );

      if (pageAskedBySource === page + 1) {
        next += `/${chapter}/${page + 1}`;
      } else if (await nextChapterExists(mangaName, chapter)) {
        next += `/${chapter + 1}/1`;
      }
    })
    .catch((error) => {
      console.log(error);
    });

  return next;
}

async function nextChapterExists(mangaName, chapter) {
  let nextChapterExists = false;

  await axios
    .get(`${BASE_URL}/${mangaName}/${chapter + 1}/1`)
    .then((response) => {
      let $ = cheerio.load(response.data);
      let $page = $(PAGE_ELEMENT);
      if ($page.length !== 0) {
        nextChapterExists = true;
      }
    })
    .catch((error) => {
      console.log(error);
    });

  return nextChapterExists;
}

async function getPreviousPage(mangaName, chapter, page) {
  let previous = `${BASE_PATH}/${mangaName}`;

  if (isFirst(page) && !isFirst(chapter)) {
    const previousChapter = isFirst(chapter) ? chapter : chapter - 1;
    const lastPage = await getLastPageOfChapter(mangaName, previousChapter);
    previous += `/${previousChapter}/${lastPage}`;
  } else if (!isFirst(page)) {
    previous += `/${chapter}/${page - 1}`;
  } else if (isFirst(page) && isFirst(chapter)) {
    previous += `/${chapter}/${page}`;
  }
  return previous;
}

async function getLastPageOfChapter(mangaName, chapter) {
  const impossiblePageToReach = 1000;
  let lastPage = 1;

  await axios
    .get(`${BASE_URL}/${mangaName}/${chapter}/${impossiblePageToReach}`)
    .then((response) => {
      lastPage = response.request.path.split("/").pop();
    })
    .catch((error) => {
      console.log(error);
    });

  return lastPage;
}

function isFirst(element) {
  return element === 1;
}
