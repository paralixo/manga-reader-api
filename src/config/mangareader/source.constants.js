export const BASE_URL = "http://mangareader.net";
export const SEARCH_PATH = "/search/?nsearch=";

export const SEARCH_ELEMENTS = {
  RESULTS: "#main .d52 .d54",
  IMAGE: ".d56",
  TITLE: ".d57 a",
  CHAPTERS: ".d58",
};

export const MANGA_ELEMENTS = {
  RESULTS: "#main",
  TITLE: ".d40",
  ALTERNATE_TITLE: ".d41 tr:nth-child(2) td:nth-child(2)",
  YEAR: ".d41 tr:nth-child(3) td:nth-child(2)",
  STATUS: ".d41 tr:nth-child(4) td:nth-child(2)",
  AUTHOR: ".d41 tr:nth-child(5) td:nth-child(2)",
  ARTIST: ".d41 tr:nth-child(6) td:nth-child(2)",
  GENRES: ".d41 tr:nth-child(8) .d42",
  DESCRIPTION: ".d46 p",
  IMAGE: ".d38 img",
  CHAPTERS: ".d48 tr:not(:first-child)",
  CHAPTER_NUMBER: "td:first-child a",
  CHAPTER_NAME: "td:first-child",
  CHAPTER_DATE: "td:nth-child(2)",
};

export const PAGE_ELEMENT = "#main .d54 img";
