import express from "express";
import axios from "axios";
import cors from "cors";

import { parseSearchView } from "./parsers/search.js";
import { parseMangaView } from "./parsers/manga.js";
import { parsePageView } from "./parsers/page.js";
import { BASE_PATH } from "./config/router.js";
import {
  BASE_URL,
  SEARCH_PATH,
} from "./config/mangareader/source.constants.js";

var app = express();
app.use(cors());

app.get(BASE_PATH, (req, res) => {
  const { search } = req.query;
  // TODO: gérer le cas où la recherche est vide (pour l'instant liste vide)

  axios
    .get(BASE_URL + SEARCH_PATH + search)
    .then((response) => {
      const mangas = parseSearchView(response.data);

      res.json(mangas);
    })
    .catch((error) => {
      console.log(error);
      res.json([]);
    });
});

app.get(`${BASE_PATH}/:name`, (req, res) => {
  const { name } = req.params;

  axios
    .get(BASE_URL + `/${name}`)
    .then((response) => {
      const manga = parseMangaView(response.data, name);

      res.json(manga);
    })
    .catch((error) => {
      console.log(error);
      res.json({});
    });
});

app.get(`${BASE_PATH}/:name/:chapter/:page`, (req, res) => {
  let { name, chapter, page } = req.params;
  chapter = parseInt(chapter);
  page = parseInt(page);

  axios
    .get(`${BASE_URL}/${name}/${chapter}/${page}`)
    .then(async (response) => {
      const pageData = await parsePageView(response.data, name, chapter, page);

      res.json(pageData);
    })
    .catch((error) => {
      console.log(error);
      res.json({});
    });
});

app.listen(3000, () => {
  console.log("Server running on http://localhost:3000/mangas");
});
