// Impossible d'utiliser la méthode .find() de cheerio dans une méthode séparée

export function parseText(element) {
  return element.text().trim();
}

export function parseImage(element, attributeName) {
  // Corrige l'url des images qui varient selon les mangas
  return parseAttribute(element, attributeName)
    .replace("//", "https://")
    .replace("https:https", "https");
}

export function parseAttribute(element, attributeName) {
  return element.attr(attributeName);
}
