# Manga reader API
> Par Florian Lafuente, Souleimane Seghir and Benoit Galmot

Il s'agit de l'API utilisé par l'application de manga reader (repo git en bas du README). Cette API est publié gratuitement sur http://florianl.eu-4.evennode.com/ jusqu'au 28 décembre 2020 (passé cette date, si besoin, veuillez nous recontacter).

Ayant pris une offre gratuite pour notre serveur, les performances de ce dernier ne sont pas incroyables. En conséquence, les temps de chargement peuvent être long par moments.

Cette API se base sur le site https://www.mangareader.net/ : on va récupérer les pages HTML qui nous intéressent, les parser et retourner les informations utilisés par l'application.

## Installation
On récupère le repo git, on installe et on lance l'API automatiquement sur localhost:3000 :
```
git clone git@gitlab.com:paralixo/manga-reader-api.git
cd manga-reader-api
npm install
npm start
```

## Routes de l'API
- `/mangas?search={nom du manga cherché}`

Liste tous les mangas qui possèdent le nom cherché (ou une partie de leur nom correspond à la valeur cherché).
Si la valeur de `search=` est vide, il va retourner les mangas par défaut de https://www.mangareader.net/ (il s'agit des mangas les plus populaires).

Exemple pour http://florianl.eu-4.evennode.com/mangas?search=punchman
```
[
    {
        name: "Onepunch-Man",
        image: "https://s3.mangareader.net/cover/onepunch-man/onepunch-man-r0.jpg",
        chapters: 229,
        url: "/onepunch-man"
    },
    {
        name: "OnePunch-Man (ONE)",
        image: "https://s3.mangareader.net/cover/onepunch-man-one/onepunch-man-one-r0.jpg",
        chapters: 104,
        url: "/onepunch-man-one"
    }
]
```

- `/mangas/{nom du manga}`

Retourne les informations concernant le manga en questions.

Exemple pour http://florianl.eu-4.evennode.com/mangas/onepunch-man : 
```
{
    title: "Onepunch-Man Manga",
    alternateTitle: "One Punch Man, onepunchman",
    year: 2012,
    status: "Ongoing",
    author: "One",
    artist: "Murata Yuusuke",
    genres: [
        "Action",
        "Comedy",
        "Seinen",
        "Supernatural"
    ],
    description: "Follows the life of an average hero who manages to win all battles with only one punch. This ability seems to frustrate him as he no longer feels the thrill and adrenaline of fighting a tough battle, which leads to him questioning his past desire of being strong.",
    image: "https://s5.mangareader.net/cover/onepunch-man/onepunch-man-l0.jpg",
    chapters: [
        {
            number: 1,
            name: "",
            date: "01/01/2013",
            link: "/mangas/onepunch-man/1/1"
        },
        {
            number: 2,
            name: "",
            date: "01/01/2013",
            link: "/mangas/onepunch-man/2/1"
        },
        {...}
    ]
}
```

- `/mangas/{nom du manga}/{numéro du chapitre}/{numéro de page}`

Retourne la page à afficher ainsi que les liens vers les pages suivantes et précédentes.

Exemple avec http://florianl.eu-4.evennode.com/mangas/onepunch-man/5/1 :
```
{
    image: "https://i8.imggur.net/onepunch-man/5/onepunch-man-3798771.jpg",
    width: 800,
    height: 1150,
    next: "/mangas/onepunch-man/5/2",
    previous: "/mangas/onepunch-man/4/21"
}
```

---
### Manga Reader APP
> https://gitlab.com/paralixo/manga-reader-app